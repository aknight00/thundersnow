class Publisher:

    def __init__(self):
        self.subscribers = []

    def register(self, callback):
        self.subscribers.append(callback)

    def unregister(self, callback):
        try:
            self.subscribers.remove(callback)
        except ValueError:
            pass
