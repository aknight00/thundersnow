# -*- coding: utf-8 -*-
from setuptools import setup

setup(name='thundersnow',
    version='0.0.1',
    description='Optimization software for differential algebraic equations',
    long_description='Optimization software for differential algebraic equations',
    classifiers=[
        'Development Status :: 3 - Alpha',
    ],
    keywords='DAE optimization MILP MINLP QP NLP MIDO IPOPT',
    url='https://bitbucket.org/loganbeal/thundersnow.git',
    author='BYU PRISM Lab',
    author_email='john_hedengren@byu.edu',
    license='MIT',
    packages=['thundersnow'],
    install_requires=[
        'numpy',
        'json',
    ],
    scripts=[
        'bin/apmonitor',
        'bin/apm.exe'
    ],
    zip_safe=False)