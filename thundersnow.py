
#%% Add package directory to system path so the apm executable is always available for local solves

"""
#TODO add "model_name." before all model parts to avoid confusion of multiple models being made at the same time
#TODO or else put every model in its own directory from the start, that keeps it separate and avoids cleanup
#TODO clean up directory better after solves

#TODO make this cross-platform
#TODO check if lib in libpath if libpath already exists
print(os.environ['LD_LIBRARY_PATH'])
if 'LD_LIBRARY_PATH' not in os.environ:
    os.environ['LD_LIBRARY_PATH'] = os.path.dirname(os.path.realpath(__file__)+'lib\\')
    #try:
    #    os.execv(sys.argv[0], sys.argv)
    #except Exception, exc:
    #    print('Failed re-exec:', exc)
    #    sys.exit(1)
if os.path.dirname(os.path.realpath(__file__)) not in sys.path:
    sys.path.append(os.path.dirname(os.path.realpath(__file__)))"""

#%% Imports
import os
import sys
import apm
import subprocess
import shutil
import json
import tempfile #making a temporary directory for all the files
#import platform
#import operator #for linking properties to automatic functions
import numpy as np #to support initializing with numpy arrays

from ts_global_options import TSGlobalOptions
from ts_parameter import TSParameter, TS_MV, TS_FV
from ts_variable import TSVariable, TS_CV, TS_SV
from ts_operators import TS_Operators
from itertools import count
#from os import path

#%% Python version compatibility
ver = sys.version_info[0]

if ver == 2:  # Python 2
    import string

    def compatible_string_strip(s):
        return string.strip(s)

else:  # Python 3+
    def compatible_string_strip(s):
        return s.strip()

def _try(o):
    try:
        return o.__dict__
    except:
        return str(o)

#%% Check for all the necessary libraries
if os.name != 'nt':
    # TODO: Check for specific required libraries once they are known
    if not os.path.isdir(os.path.join(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'bin'),'lib')):
        print('Warning: \'lib\' folder is missing. Necessary libraries may not be present.')


#%% Equation Object Class, to allow referencing equation later
class EquationObj(object):
    def __init__(self,value):
        self.value = str(value)
    def __str__(self):
        return self.value
        
#%%Create class
class ThunderSnow(object):
    """Create a model object. This is the basic object for solving optimization problems"""
    _ids = count(0) #keep track of number of active class instances to not overwrite eachother with default model name

    def __init__(self, server='http://xps.apmonitor.com', name=None, remote=False):
        self.server = compatible_string_strip(server)
        self.options = TSGlobalOptions()
        self.id = next(self._ids) #instance count of class
        #self.options.register(self.set_global_option) #this looks like it's just for remote solves?

        #TODO make backend properties (constant, variables, etc) hidden with prepended _ underscore
        #keep a list of constants, params, vars, eqs, etc associated with this model
        self._constants = []
        self.parameters = []
        self.variables = []
        self.intermediates = []
        self.inter_equations = []
        self.equations = []
        self.objectives = [] #what does this do?

        #time discretization
        self.time = None

        self.model_initialized = False
        self.saved_model_options = [] #what does this do?
        self.csv_status = None #indicate 'provided' or 'generated'
        #TODO Allow user to load custom .apm model file
        self.model = ''

        #Default model name, numbered to allow multiple models
        if name == None:
            name = 'ts_model'+str(self.id)
        self.model_name = name.lower().replace(" ", "")#TODO consider separate "app" name for remote solves
        #Path of model folder
        self.path = tempfile.mkdtemp(suffix=self.model_name)
        
        #Create and open configuration files
        self.f_info = open(os.path.join(self.path,self.model_name)+'.info', 'w+') #for classifiying variables



        
    #%% Parts of the model
    def Const(self, value=0, name=''):
        """ Define a constant. There is no functional difference between using
        this Const, a python variable or a magic number. However, this Const
        can be provided a name to make the .apm model more clear."""
        #TODO value cannot be an array
        const = TS_Operators(name,value)
        self._constants.append(const)
        return const

#TODO provide option [integer=False]; prepend name with 'int'
    def Param(self, name='', value=0, lb=None, ub=None, integer=False):
        """TS parameters can become MVs and FVs. Since ThunderSnow defines
        MVs and FVs directly, there's not much use for parameters. Parameters
        are effectively constants unless the resulting .spm model is used later
        and the parameters can be set as MVs or FVs. """
        if name == '':
            name = 'p' + str(len(self.parameters) + 1)

        parameter = TSParameter(name, value, lb, ub)
        self.parameters.append(parameter)
        return parameter

    def FV(self, name='',value=0, lb=None, ub=None, integer=False):
        """A manipulated variable that is fixed with time. Therefore it lacks
        time-based attributes."""
        if name == '':
            name = 'p' + str(len(self.parameters) + 1)
            if integer == True:
                name = 'int_'+name

        parameter = TS_FV(name=name, value=value, lb=lb, ub=ub, ts_model=self.model_name, model_path=self.path, integer=integer) 
        parameter.register(self.set_model_option)
        self.parameters.append(parameter)
        #Classify variable in .info file
        self.f_info.write('F, '+name+'\n')
        #self.saved_model_options.append('info FV, {0}'.format(name))
        return parameter

    def MV(self, name='', value=0, lb=None, ub=None, integer=False):
        """Change these variables optimally to meet objectives"""
        if name == '':
            name = 'p' + str(len(self.parameters) + 1)
            if integer == True:
                name = 'int_'+name

        parameter = TS_MV(name=name, value=value, lb=lb, ub=ub, ts_model=self.model_name, model_path=self.path, integer=integer)
        parameter.register(self.set_model_option)
        self.parameters.append(parameter)
        #Classify variable in .info file
        self.f_info.write('M, '+name+'\n')
        #self.saved_model_options.append('info MV, {0}'.format(name))
        return parameter

    def Var(self, name='', value=0, lb=None, ub=None, integer=False):
        """Calculated by solver to meet constraints (Equations). The number of
        variables (including CVs and SVs) must equal the number of equations."""
        if name == '':
            name = 'v' + str(len(self.variables) + 1)
            if integer == True:
                name = 'int_'+name

        variable = TSVariable(name, value, lb, ub)
        self.variables.append(variable)
        return variable

    def SV(self, name='', value=0, lb=None, ub=None, integer=False):
        """A variable that's special"""
        if name == '':
            name = 'v' + str(len(self.variables) + 1)
            if integer == True:
                name = 'int_'+name

        variable = TS_SV(name=name, value=value, lb=lb, ub=ub, ts_model=self.model_name, model_path=self.path, integer=integer)
        variable.register(self.set_model_option)
        self.variables.append(variable)
        #Classify variable in .info file
        self.f_info.write('S, '+name+'\n')
        #self.saved_model_options.append('info SV, {0}'.format(name))
        return variable

    def CV(self, name='', value=0, lb=None, ub=None, integer=False):
        """A variable with a setpoint. Reaching the setpoint is added to the
        objective."""
        if name == '':
            name = 'v' + str(len(self.variables) + 1)
            if integer == True:
                name = 'int_'+name

        variable = TS_CV(name=name, value=value, lb=lb, ub=ub, ts_model=self.model_name, model_path=self.path, integer=integer)
        variable.register(self.set_model_option)
        self.variables.append(variable)
        #Classify variable in .info file
        self.f_info.write('C, '+name+'\n')
        #self.saved_model_options.append('info CV, {0}'.format(name))
        return variable

    def Inter(self,name=''):
        inter = TS_Operators(name)
        self.intermediates.append(inter)
        return inter

    def Inter_EQ(self,equation):
        self.inter_equations.append(str(equation))

    def Equation(self,equation):
        EqObj = EquationObj(equation)
        self.equations.append(EqObj)
        return EqObj
        
    def Equations(self,eqs):
        l = []        
        for eq in eqs:
            eo = self.Equation(eq)
            l.append(eo)
        return l

    def Obj(self,obj):
        self.objectives.append('minimize ' + str(obj))


    #%% Add array functionality to all types
    def Array(self,f,dim,**args):
        x = np.ndarray(dim,dtype=object)
        for i in np.nditer(x, flags=["refs_ok"],op_flags=['readwrite']):
            i[...] = f(**args)
        return x
    """
    #gives and array in a list instead of numpy ndarray
    def Arraylist(sizes, f):
        if (len(sizes) == 1):
            return [f()] * sizes[0]
        else:
            return [init(sizes[1:], f) for i in xrange(sizes[0])]           
    """        
    #%% Get a solution
    def solve(self,remote=False,disp=True):
        """Solve the optimization problem.

        This function has these substeps:
        -Validates the model and write .apm file (if .apm not supplied)
        -Validate and write .csv file (if none provided)
        -Write options to overrides.dbs
        -Solve the problem using the apm.exe commandline interface.
        -Load results into python variables.
        """

        #TODO Validate model for:
            #matching vars to eqs
            #invalid (reserved keywords) in variables
            #correct imode

        import time
        
        t = time.time()
        self.to_JSON()
        print('print JSON', time.time() - t)
        
        
        t = time.time()
        #TODO make flags like this (self.model='provided') in function that take .apm and .csv files
        # Build the model
        if self.model != 'provided': #no model was provided
            self.build_model()

        print('build model', time.time() - t)


        t = time.time()
        if self.time is not None and self.csv_status != 'provided': #TODO should these checks be in the write_csv function to simplify the solve() function?
            self.write_csv()
        print('build csv', time.time() - t)

        t = time.time()
        self.generate_overrides_dbs_file()
        print('build overrides', time.time() - t)


        t = time.time()
        #Close files for writing
        self.f_info.close()
        print('close files', time.time() - t)

        if remote == False:#local_solve
            #TODO add other parameters http://apmonitor.com/wiki/index.php/Main/CommandLine
            t = time.time()
            # Calls apmonitor through the command line
            if os.name == 'nt': #Windows
                apm_exe = os.path.join(os.path.dirname(os.path.realpath(__file__)),'bin/apm.exe')
                print(apm_exe)
                print(self.path)
                app = subprocess.Popen([apm_exe, self.model_name], stdout=subprocess.PIPE, stderr=subprocess.PIPE,cwd = self.path, env = {"PATH" : self.path }, universal_newlines=True)
                if disp == True:
                    for line in iter(app.stdout.readline, ""):
                        try:
                            print(line.replace('\n', ''))
                        except:
                            pass
                app.wait()
            else:
                apm_exe = os.path.join(os.path.dirname(os.path.realpath(__file__)),'bin/apmonitor')
                app = subprocess.Popen([apm_exe, self.model_name], stdout=subprocess.PIPE, stderr=subprocess.PIPE,cwd = self.path, env = {"PATH" : self.path, "LD_LIBRARY_PATH" : os.path.dirname(os.path.realpath(__file__))+'/bin/lib' }, universal_newlines=True)
                
                for line in iter(app.stdout.readline, ""):
                    if disp == True:
                        print(line.replace('\n', ''))
                app.wait()
            out, errs = app.communicate()
            # print(out)
            if errs:
                print("Error:", errs)
            print('solve', time.time() - t)
            
        else: #solve on APM server
            #clear anything already on the server
            apm.cmd(self.server,self.model_name,'clear all')
            #send model file
            f = open(os.path.join(self.path,self.model_name + '.apm'))
            model = f.read()
            f.close()
            apm.cmd(self.server, self.model_name, ' '+model)
            #send csv file
            f = open(os.path.join(self.path,self.model_name + '.csv'))
            csv = f.read()
            f.close()
            apm.cmd(self.server, self.model_name, 'csv '+csv)
            #send info file
            f = open(os.path.join(self.path,self.model_name + '.info'))
            info = f.read()
            f.close()
            apm.cmd(self.server, self.model_name, 'info '+info)
            #send dbs file
            f = open(os.path.join(self.path,'overrides.dbs'))
            dbs = f.read()
            f.close()
            apm.cmd(self.server, self.model_name, 'option '+dbs)
            
            #solve remotely
            apm.cmd(self.server, self.model_name, 'solve')
            
            #load results
            results = apm.get_file(self.server,self.model_name,'results.csv')
            f = open(os.path.join(self.path,'results.csv'), 'w')
            f.write(results)
            f.close() 
            results = apm.get_file(self.server,self.model_name,'results.json')
            f = open(os.path.join(self.path,'results.json'), 'w')
            f.write(results)
            f.close() 
            options = apm.get_file(self.server,self.model_name,'options.json')
            f = open(os.path.join(self.path,'options.json'), 'w')
            f.write(options)
            f.close() 
        
        print('solve', time.time() - t)
        
        t = time.time()
        self.load_results() #TODO check this
        print('load results', time.time() - t)
        # Clean the directory
        #self.clean_directory()
        
        t = time.time()
        self.load_JSON()
        print('load JSON', time.time() - t)
        
        t = time.time()
        self.verify_input_options()
        print('compare options', time.time() - t)
        
        if os.path.isfile(os.path.join(os.path.dirname(os.path.realpath(__file__)),'infeasibilities.txt')):
            raise StopIteration('Infeasible problem! Solution not trustworthy.')

    #%% Write files

    def jsonify(self,data): #This function was mostly copied from SO
        if type(data).__module__=='numpy': # if value is numpy.*: > to python list
            json_data = data.tolist()
        elif isinstance(data, dict): # for nested lists
            json_data = dict()
            for key, value in data.iteritems():
                if isinstance(value, list): # for lists
                    value = [ self.jsonify(item) if isinstance(item, dict) else item for item in value ]
                if isinstance(value, dict): # for nested lists
                    value = self.jsonify(value)
                if isinstance(key, int): # if key is integer: > to string
                    key = str(key)
                if type(value).__module__=='numpy': # if value is numpy.*: > to python list
                    value = value.tolist()
                json_data[key] = value
        else:
            json_data = data
        return json_data
    
    
    def to_JSON(self):
        """
        include in JSON:
        global options
        variables (const,param,inter,var)
            name
            value            
            type
            option_list
        """
        
        json_data = dict()
        #global options
        o_dict = dict()
        for o in self.options._input_option_list+self.options._inout_option_list:
            o_dict[o] = getattr(self.options,o)
        json_data['global options'] = o_dict
        
        if self.time != None:
            json_data['time'] = self.jsonify(self.time)
        #Constants can't and won't change so there's no reason to pass them in the JSON
        #constant values must be given in the model file. Changing constant values requires recompiling the model.
#        #constants
#        if self._constants:
#            const_dict = dict()
#            for const in self._constants:
#                const_dict['value'] = self.jsonify(const.value)
#            const_dict[const.name] = const_dict

        if self.parameters:
            p_dict = dict()
            for parameter in self.parameters:
                temp_dict = dict()
                temp_dict['type'] = parameter.type
                temp_dict['value'] = self.jsonify(parameter.value)
                if parameter.type != None:
                    o_dict = dict()
                    for o in parameter.option_list:
                        o_dict[o] = getattr(parameter,o)
                    temp_dict['options'] = o_dict
                #TODO does changing the hard upper/lower variable constraints require recompiling the model?
#                temp_dict['upper_bound'] = parameter.ub
#                temp_dict['lower_bound'] = parameter.lb
                p_dict[parameter.name] = temp_dict
            json_data['parameters'] = p_dict
        
        if self.variables:
            p_dict = dict()
            for parameter in self.variables:
                temp_dict = dict()
                temp_dict['type'] = parameter.type
                temp_dict['value'] = self.jsonify(parameter.value)
                if parameter.type != None:
                    o_dict = dict()
                    for o in parameter.option_list:
                        o_dict[o] = getattr(parameter,o)
                    temp_dict['options'] = o_dict
                #TODO does changing the hard upper/lower variable constraints require recompiling the model?
#                temp_dict['upper_bound'] = parameter.ub
#                temp_dict['lower_bound'] = parameter.lb
                p_dict[parameter.name] = temp_dict
            json_data['variables'] = p_dict

#TODO do intermediates need values? they are just names right now
        """
        if self.intermediates:
            temp_dict = dict()
            for intermediate in self.intermediates:
                temp_dict['name'] = {'value':self.jsonify(intermediate.value)}
            json_data['intermediates'] = temp_dict
        """        
        f = open(os.path.join(self.path,'jsontest.json'), 'w')
        #f.write(json.dumps(self, default=lambda o: _try(o), sort_keys=True, indent=2, separators=(',',':')).replace('\n', ''))
        json.dump(json_data,f, indent=2,separators=(',', ':'))     
        f.close()
        #return json.dumps(self, default=lambda o: _try(o), sort_keys=True, indent=0, separators=(',',':')).replace('\n', '')
        #load JSON to dictionary: 
        #with open(os.path.join(self.path,'jsontest.json')) as json_file:
        #   data = json.load(json_file)
        

        
    def build_model(self):
        ''' Write model to apm file.

        Also does some minimal model validation

        Returns:
            Does not return
        '''
        #check model size
        if len(self.variables) != len(self.equations):
            print('BAD. # variables must match # equations.')
            print('maybe we should move things around for them?')
        if len(self.intermediates) != len(self.inter_equations):
            print('Each intermediate needs and equation')
            # '%s = %f' % (self.name, self.value)

        model = ''

        if self._constants:
            model += 'Constants\n'
            for const in self._constants:
                model += '\t%s = %s\n' % (const, const.value)
            model += 'End Constants\n'
#TODO double-check if commas are needed. it would be prettier without them
        if self.parameters:
            model += 'Parameters\n'
            for parameter in self.parameters:
                i = 0
                model += '\t%s' % parameter
                if not isinstance(parameter.value, (list,np.ndarray)):
                    i = 1
                    model += ' = %s' % parameter.value
                if parameter.ub != None:
                    if i == 1:
                        model += ', '
                    i = 1
                    model += '<= %s' % parameter.ub
                if parameter.lb is not None:
                    if i == 1:
                        model += ', '
                    i = 1
                    model += '>= %s' % parameter.lb
                model += '\n'
            model += 'End Parameters\n'

        if self.variables:
            model += 'Variables\n'
            for parameter in self.variables:
                i = 0
                model += '\t%s' % parameter
                if not isinstance(parameter.value, (list,np.ndarray)):
                    i = 1
                    model += ' = %s' % parameter.value
                if parameter.ub != None:
                    if i == 1:
                        model += ', '
                    i = 1
                    model += '<= %s' % parameter.ub
                if parameter.lb is not None:
                    if i == 1:
                        model += ', '
                    i = 1
                    model += '>= %s' % parameter.lb
                model += '\n'
            #TODO make the parameter/variable writing prettier (a function?) to avoid duplicate code
                """
            for variable in self.variables:
                model += '\t%s = %s' % (variable, variable.value)
                if variable.ub is not None:
                    model += ', <= %s' % variable.ub
                if variable.lb is not None:
                    model += ', >= %s' % variable.lb
                model += '\n'
                """
            model += 'End Variables\n'

        if self.intermediates:
            model += 'Intermediates\n'
            for intermediate in self.inter_equations:
                model += '\t%s\n' % intermediate
            model += 'End Intermediates\n'

        if self.equations:
            model += 'Equations\n'
            for equation in self.equations:
                model += '\t%s\n' % equation
            if self.objectives:
                for o in self.objectives:
                    model += '\t%s\n' % o
            model += 'End Equations'

        #print(model) #mostly for debugging

        # Create .apm file
        if(self.model_name == None):
            self.model_name = "default_model_name"
        filename = self.model_name + '.apm'

        # if not(path.isfile(file_name)):
        # Create file in writable format always overrite previous model file
        f = open(os.path.join(self.path,filename), 'w')
        f.write('Model\n')
        f.write(model)
        f.write('\nEnd Model')
        f.close()

        self.model = 'auto-generated' #what does this do?

        self.model_initialized = True

        ############## What is this doing?
        #for command in self.saved_model_options:
        #    apm.cmd(self.server, self.app, command)

        #self.saved_model_options = []


    def write_csv(self):
        """Write csv file and validate data.
        If the problem is dynamic, the time discretization is provided in the
        first column of this csv. All params/variables that are initialized
        with an array are loaded as well and must be the same length. """

        file_name = self.model_name + '.csv'

        #TODO only require time vairable if imode > 3
        #Start with time
        length = np.size(self.time)
        csv_data = np.hstack(('time',np.array(self.time).flatten()))

        #check all parameters and arrays
        for vp in self.variables+self.parameters:
            #only inlcude arrays (non-arrays are initialized in .apm)
            if isinstance(vp.value, (list,np.ndarray)):
                if np.size(vp.value) != length:
                    raise Exception('Data points must match time discretization')
                #add a new row with the variable name and the data array
                t = np.hstack((str(vp),np.array(vp.value).flatten()))
                csv_data = np.vstack((csv_data,t))

        #save array to csv
        np.savetxt(os.path.join(self.path,file_name), csv_data.T, delimiter=",", fmt='%s')
        self.csv_status = 'generated'

    def generate_overrides_dbs_file(self):
        '''Write options to overrides.dbs file

        Returns:
            Does not return
        '''
        filename = 'overrides.dbs'
        file_content = self.options.getOverridesString()

        f = open(os.path.join(self.path,filename), 'a')
        f.write(file_content)
        f.close()

    #%% Post-solve processing

    def load_JSON(self):
        f = open(os.path.join(self.path,'options.json'))
        data = json.load(f)
        f.close()
        for o in self.options._output_option_list:
            self.options.__dict__[o] = data['APM'][o]
        for o in self.options._inout_option_list:
            self.options.__dict__[o] = data['APM'][o]
        return data
        
    def load_results(self):
        if (os.path.isfile(os.path.join(self.path, 'results.csv'))):
            f = open(os.path.join(self.path,'results.json'))
            data = json.load(f)
            f.close()
            
            for vp in self.parameters+self.variables:
                try:
                    vp.value = data[vp.name]
                except Exception:
                    print(vp.name+ " not found in results file")
            
            return data
            
        else:
            print("Error: 'results.json' not found. Check above for addition error details")
            return {}
            

                
        print(data['APM']['SOLVESTATUS'])
        return data
    
    def verify_input_options(self):
        f = open(os.path.join(self.path,'options.json'))
        data = json.load(f)
        f.close()
        for o in self.options._input_option_list:
            if self.options.__dict__[o] != data['APM'][o]:
                print(str(o)+" was not written correctly") 
        
        
    def load_csv_results(self):
        # Add time as a parameter
        #TODO why add time as a paramter? this gives multiple time variables when resolving
        #self.Param('time')
        #TODO does time need to be loaded back in? Would time ever be in results.csv but not self.time?

        # Load results.csv into a dictionary keyed with variable names
        if (os.path.isfile(os.path.join(self.path, 'results.csv'))):
            with open(os.path.join(self.path,'results.csv')) as f: #TODO maybe this should be moved to subfolder then loaded so this functionality persists after we move it
                reader = apm.csv.reader(f, delimiter=',')
                y={}
                for row in reader:
                    if len(row)==2:
                        y[row[0]] = float(row[1])
                    else:
                        y[row[0]] = [float(col) for col in row[1:]]
            # Load variable values into their respective objects from the dictionary
            for vp in self.parameters+self.variables:
                try:
                    vp.value = y[str(vp)]
                except Exception:
                    pass
            # Return solution
            return y

        else:
            print("Error: 'results.csv' not found. Check above for addition error details")
            return {}




    def clean_directory(self): #TODO this whole function probably isn't needed any more, it's from when files were all made in root TS directory, then moved after the solve
        ''' Cleans local directory of files created by apm.exe

            When apm.exe is executed, many files are returned to the local
            directory. This function creates a sub-directory and moves all files
            except the original python script and .csv file into the
            sub-directory.
        '''
        # Name of new directory
        dir_name = self.model_name

        # Make directory with app name for moving files
        if os.name == 'nt': #Windows
            subprocess.call(["mkdir", dir_name],shell=True)
        else:
            subprocess.call(["mkdir", dir_name])#,shell=True)
        #TODO shell=True doesn't work on Linux. Check importance on other platforms


        # Move all .htm, .hst, .model, .dbs, and .xml files to new directory
        # Array for file types to move to new directory
        file_type_list = ('.htm', '.hst', '.model', '.xml', '.info', '.mdf',
                          '.nlc', '.php', '.out', '.rpt', '.rt0', '.rtO', '.t0',
                          '.apm','.dbs','.txt','.csv','.newval','.dxdt')

        for file in os.listdir(os.path.dirname(os.path.realpath(__file__))):
            if file.endswith(file_type_list):
                if os.name == 'nt': #Windows
                    subprocess.call(["move", os.path.join(os.path.dirname(os.path.realpath(__file__)), file), os.path.join(os.path.dirname(os.path.realpath(__file__)), dir_name, file)],shell=True)
                else:
                    os.rename(os.path.join(os.path.dirname(os.path.realpath(__file__)), file), os.path.join(os.path.dirname(os.path.realpath(__file__)), dir_name, file))


    def clear(self):
        #If the directory exists (a previous model used the same name)
        if os.path.isdir(self.path):
            #Delete the directory and its contents
            shutil.rmtree(self.path,ignore_errors=True)

    #%% Trig functions
    def sin(self,other):
        return TS_Operators('sin(' + str(other) + ')')
    def cos(self,other):
        return TS_Operators('cos(' + str(other) + ')')
    def tan(self,other):
        return TS_Operators('tan(' + str(other) + ')')
    def sinh(self,other):
        return TS_Operators('sinh(' + str(other) + ')')
    def cosh(self,other):
        return TS_Operators('cosh(' + str(other) + ')')
    def tanh(self,other):
        return TS_Operators('tanh(' + str(other) + ')')
    def exp(self,other):
        return TS_Operators('exp(' + str(other) + ')')
    def log(self,other):
        return TS_Operators('log('+str(other) + ')')
    def sqrt(self,other):
        return TS_Operators('sqrt('+str(other) + ')')

    #%%








    #%% Alternatives to recommended functions

    #I don't think we need this anymore
    def set_global_option(self, name, value):
        command = 'option {0} = {1}'.format(name, value)
        apm.cmd(self.server, self.app, command)

    def set_model_option(self, name, value):
        command = 'option {0} = {1}'.format(name, value)

        if self.model_initialized:
            apm.cmd(self.server, self.app, command)
        else:
            self.saved_model_options.append(command)

    def set_model_name(self, mod_name):
        ''' Set the model name

            The model name will be used to name the .apm file that will be
            generated. For example, if model_name = 'demo', then the model
            file will be called "demo.apm"

            Args:
                mod_name: String model name
        '''
        self.model_name = mod_name


    def get_parameter(self, name):
        """Find and return parameter from model.

        Args:
            name: Name of parameter.

        Returns:
            The Parameter if it exists. It the parameter does
            not exist, then None is returned.
        """
        for vp in self.parameters:
            if str(vp) == str(name):
                return vp
        return None



    def add_parameter(self, name='', value=0, param_type=''):
        if name == '':
            name = 'p' + str(len(self.parameters) + 1)

        param_type = param_type.lower()

        if param_type == 'fv' or param_type == 'f':
            parameter = TS_FV(name, value)
            parameter.register(self.set_model_option)
            self.saved_model_options.append('info FV, {0}'.format(name))
        elif param_type == 'mv' or param_type == 'm':
            parameter = TS_MV(name, value)
            parameter.register(self.set_model_option)
            self.saved_model_options.append('info MV, {0}'.format(name))
        else:
            parameter = TSParameter(name, value)

        self.parameters.append(parameter)
        return parameter

    def add_variable(self, name, value, var_type):
        if name == '':
            name = 'v' + str(len(self.variables) + 1)

        if var_type == 'sv' or var_type == 's':
            variable = TS_SV(name, value)
            self.saved_model_options.append('info SV, {0}'.format(name))
        elif var_type == 'cv' or var_type == 'c':
            variable = TS_CV(name, value)
            self.saved_model_options.append('info CV, {0}'.format(name))
        else:
            variable = TSVariable(name, value)

        self.variables.append(variable)
        return variable

    def add_intermediate_equation(self, intermediate):
        self.intermediates.append(intermediate)

    def add_equation(self, equation):
        self.equations.append(equation)

    def load_data(self, headers, data):
        data_string = 'csv ' + ','.join(headers) + '\n'

        for i in range(len(data[0])):
            data_string += ','.join(data[:, i])

        apm.cmd(self.server, self.app, data_string)

    def load_data_from_file(self, filename):
        apm.load_data(self.server, self.app, filename)


    def load_measurement(self, name, value):########## Need local version?#################33
        return apm.load_meas(self.server, self.app, name, value)

    def get_attribute(self, name):
        return apm.get_attribute(self.server, self.app, name)




    # TODO: if .dt() used and csv not provided, demand time variable
