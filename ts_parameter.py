import os 
from ts_operators import TS_Operators
from publisher import Publisher


class TSParameter(TS_Operators):
    """Represents a parameter in a model."""
    counter = 1

    def __init__(self, name='', value=0, lb=None, ub=None, integer=False):
        if name == '':
            name = 'p' + TSParameter.counter
            TSParameter.counter += 1
            if integer == True:
                name = 'int_' + name

        TS_Operators.__init__(self, name)

        self.value = value #initialized value
        self.type = None #TODO is this needed?
        self.lb = lb #lower bound
        self.ub = ub #upper bound

    def __repr__(self):
        return str(self.value) #'%s = %f' % (self.name, self.value)


# TODO: check if OSTATUS, OSTATUSCHG, PRED, DPRED, AWS, VLACTION, TIER are correctly exposed

FV_option_list = ['CRITICAL', 'DMAX', 'DMAXHI', 'DMAXLO', 'FSTATUS', 'LOWER',
                  'LSTVAL', 'MEAS', 'NEWVAL', 'OSTATUS', 'OSTATUSCHG', 'PSTATUS',
                  'STATUS', 'UPPER', 'VDVL', 'VLACTION', 'VLHI', 'VLLO']

#TODO why do FV MV CS SV need Publisher? I think I removed the need for it with the modified __setattr_ writing to file directly
class TS_FV(TSParameter, Publisher):
    """Fixed Variable. Inherits TSParameter and Publisher."""

    def __init__(self, name='', value=0, lb=None, ub=None, ts_model=None, model_path=None, integer=False):
        # prevents the __setattr__ function from sending options to the server
        # until the __init__ function has completed since they should only be
        # sent if changed from their defaults
        self.initialized = False

        TSParameter.__init__(self, name, value, lb, ub, integer)
        Publisher.__init__(self)

        self.type = 'FV'
        self.option_list = FV_option_list
        self.model_name = ts_model
        #self.path = os.path.join(os.path.dirname(os.path.realpath(__file__)), self.model_name) OLD from when model file were in the same directory; now using temp files
        self.path = model_path #use the same path as the model 
        
        
        # FV options
        self.CRITICAL = 0
        self.DMAX = 1.0e20
        self.DMAXHI = 1.0e20
        self.DMAXLO = -1.0e20
        self.FSTATUS = 1.0
        self.LOWER = -1.0e20
        self.LSTVAL = 1.0
        self.MEAS = 1.0
        self.NEWVAL = 1.0
        self.OSTATUS = 0
        self.OSTATUSCHG = 0
        self.PSTATUS = 1
        self.STATUS = 1
        self.UPPER = 1.0e20
        self.VDVL = 1.0e20
        self.VLACTION = 0
        self.VLHI = 1.0e20
        self.VLLO = -1.0e20

        # now allow options to be sent to the server
        self.initialized = True

    def __setattr__(self, name, value):
        self.__dict__[name] = value

        # When an option is changed, notify subscribers
        if self.initialized and name in self.option_list:
            f = open(os.path.join(self.path,'overrides.dbs'),'a')
            f.write(self.name+'.'+name+' = '+str(value)+'\n')
            f.close()
            #for callback in self.subscribers:
                #callback('{0}.{1}'.format(self.name, name), value)

    def meas(self,measurement):
        #open measurement.dbs file
        f = open(os.path.join(self.path,'measurements.dbs'),'a')
        #write measurement
        f.write(self.name+'.MEAS = '+str(measurement)+', 1, none\n')
        #close file
        f.close()
        
        #write tag file
        f = open(os.path.join(self.path,self.name),'w')
        #write measurement
        f.write(str(measurement))
        #close tag file
        f.close()
        
        
        
        
MV_option_list = FV_option_list + ['AWS', 'COST', 'DCOST', 'DPRED', 'MV_STEP_HOR',
                                   'NXTVAL', 'PRED', 'REQONCTRL', 'TIER']


class TS_MV(TS_FV):
    """ Manipulated Variable. Inherits TS_FV."""

    def __init__(self, name='', value=0, lb=None, ub=None, ts_model=None, model_path=None, integer=False):
        TS_FV.__init__(self, name=name, value=value, lb=lb, ub=ub, ts_model=ts_model, model_path=model_path, integer=integer)

        # prevents the __setattr__ function from sending options to the server
        # until the __init__ function has completed since they should only be
        # sent if changed from their defaults
        self.initialized = False

        self.type = 'MV'
        self.option_list = MV_option_list

        # options for manipulated variables
        self.AWS = 0
        self.COST = 0.0
        self.DCOST = 0.00001
        self.DPRED = 0.0
        self.MV_STEP_HOR = 0
        self.NXTVAL = 1.0
        self.PRED = 1.0
        self.REQONCTRL = 0
        self.TIER = 1

        # now allow options to be sent to the server
        self.initialized = True
        
    def dt(self):
        return TS_Operators('$' + self.name)
