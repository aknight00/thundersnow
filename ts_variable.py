import os
from ts_operators import TS_Operators
from publisher import Publisher


class TSVariable(TS_Operators):
    """Represents a parameter in a model"""
    counter = 0
    
    def __init__(self, name='', value=0, lb=None, ub=None, integer=False):
        if name == '':
            name = 'v' + TSVariable.counter
            TSVariable.counter += 1
            if integer == True:
                name = 'int_'+name

        TS_Operators.__init__(self, name)

        self.value = value #initialized value
        self.type = None #TODO is this needed?
        self.lb = lb #lower bound
        self.ub = ub #upper bound

    def dt(self):
        return TS_Operators('$' + self.name)

    def __repr__(self):
        return str(self.value) #'%s = %f' % (self.name, self.value)


# TODO: (???) check if OSTATUS, OSTATUSCHG, MEAS_GAP, PRED, DPRED, BIAS, TIER, VLACTION, WSP are correctly exposed

SV_option_list = ['FSTATUS', 'LOWER', 'MEAS', 'MODEL', 'PRED', 'UPPER']


class TS_SV(TSVariable, Publisher):
    """State Variable. Inherits TSVariable."""

    def __init__(self, name='', value=0, lb=None, ub=None, ts_model=None, model_path=None, integer=False):
        # prevents the __setattr__ function from sending options to the server
        # until the __init__ function has completed since they should only be
        # sent if changed from their defaults
        self.initialized = False

        TSVariable.__init__(self, name, value, lb, ub, integer)
        Publisher.__init__(self)

        self.type = 'SV'
        self.option_list = SV_option_list
        self.model_name = ts_model 
        #self.path = os.path.join(os.path.dirname(os.path.realpath(__file__)), self.model_name) OLD from when model file were in the same directory; now using temp files
        self.path = model_path #use the same path as the model 
        
        # SV specific options
        self.FSTATUS = 0
        self.LOWER = -1.0e20
        self.MEAS = 1.0
        self.MODEL = 1.0
        self.PRED = 1.0
        self.UPPER = 1.0e20

        # now allow options to be sent to the server
        self.initialized = True
    """
    def __setattr__(self, name, value):
        self.__dict__[name] = value

        # When an option is changed, use the callback
        if self.initialized and name in self.option_list:
            for callback in self.subscribers:
                callback('{0}.{1}'.format(self.name, name), value)
    """
    def __setattr__(self, name, value):
        self.__dict__[name] = value

        # When an option is changed, notify subscribers
        if self.initialized and name in self.option_list:
            f = open(os.path.join(self.path,'overrides.dbs'),'a')
            f.write(self.name+'.'+name+' = '+str(value)+'\n')
            f.close()
            #for callback in self.subscribers:
                #callback('{0}.{1}'.format(self.name, name), value)
            


CV_option_list = SV_option_list + ['BIAS', 'COST', 'CRITICAL', 'FDELAY', 'LSTVAL',
                                   'MEAS_GAP', 'OSTATUS', 'OSTATUSCHG', 'PSTATUS',
                                   'SP', 'SPHI', 'SPLO', 'STATUS', 'TAU', 'TIER',
                                   'TR_INIT', 'TR_OPEN', 'VDVL', 'VLACTION', 'VLHI',
                                   'VLLO', 'WMEAS', 'WMODEL', 'WSP', 'WSPHI', 'WSPLO']


class TS_CV(TS_SV):
    """Controlled Variable. Inherits variable """
    
    def __init__(self, name='', value=0, lb=None, ub=None, ts_model=None, model_path=None, integer=False):
        TS_SV.__init__(self, name=name, value=value, lb=lb, ub=ub, ts_model=ts_model, model_path=model_path, integer=integer)
        # prevents the __setattr__ function from sending options to the server
        # until the __init__ function has completed since they should only be
        # sent if changed from their defaults
        self.initialized = False

        self.type = 'CV'
        self.option_list = CV_option_list
        
        # CV specific options
        self.BIAS = 0.0
        self.COST = 0.0
        self.CRITICAL = 0
        self.FDELAY = 0
        self.LSTVAL = 1.0
        self.MEAS_GAP = 1.0e-3
        self.OSTATUS = 0
        self.OSTATUSCHG = 0
        self.PSTATUS = 1
        self.SP = 0.0
        self.SPHI = 1.0e20
        self.SPLO = -1.0e20
        self.STATUS = 0
        self.TAU = 60.0
        self.TIER = 1
        self.TR_INIT = 1
        self.TR_OPEN = 1.0
        self.VDVL = 1.0e20
        self.VLACTION = 0
        self.VLHI = 1.0e20
        self.VLLO = -1.0e20
        self.WMEAS = 20.0
        self.WMODEL = 2.0
        self.WSP = 20.0
        self.WSPHI = 20.0
        self.WSPLO = 20.0

        # now allow options to be sent to the server
        self.initialized = True

    def meas(self,measurement):
        #open measurement.dbs file
        f = open(os.path.join(self.path,'measurements.dbs'),'a')
        #write measurement
        f.write(self.name+'.MEAS = '+str(measurement)+', 1, none\n')
        #close file
        f.close()
        
        #write tag file
        f = open(os.path.join(self.path,self.name),'w')
        #write measurement
        f.write(str(measurement))
        #close tag file
        f.close()
